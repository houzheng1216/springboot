package com.edu.spring.applicationcontext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class OneBean {
    @Autowired  //依赖注入,方式一
    private ApplicationContext applicationcontext;

    public void show() {
        System.out.println("----" + applicationcontext);
    }

}
