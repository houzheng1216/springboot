package com.edu.spring.applicationcontext;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

//实现BeanPostProcessor接口的方法: 会在容器中每个Bean初始化的时候调用一次
// 这里可以对指定的bean做一些处理,比如,反悔bean的代理对象
@Component
public class EchoBeanPostProcess implements BeanPostProcessor {

    //在Bean 的init方法执行之前执行
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("---------------");
        return bean;//不能反悔null
    }

    //在Bean 的init方法执行之后执行,在依赖完成后(即属性注入set)执行
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("-----------------");
        return bean;
    }

}
