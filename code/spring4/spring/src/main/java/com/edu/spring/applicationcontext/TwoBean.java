package com.edu.spring.applicationcontext;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class TwoBean implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    //继承接口,实现接口方法,获取
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public void show() {
        System.out.println("----" + applicationContext);
    }


}
