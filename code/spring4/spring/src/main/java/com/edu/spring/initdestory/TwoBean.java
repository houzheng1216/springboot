package com.edu.spring.initdestory;

public class TwoBean {

    public void init() {
        System.out.println("对象初始化----");
    }

    public void destroy() {
        System.out.println("对象销毁-----");
    }

}
