package com.edu.spring.initdestory;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MyConfig.class);

        OneBean oneBean = context.getBean(OneBean.class);
        System.out.println(oneBean);

        TwoBean twoBean = context.getBean(TwoBean.class);
        System.out.println(twoBean);

        ThreeBean threeBean = context.getBean(ThreeBean.class);
        System.out.println(threeBean);
        context.close();
    }

}
