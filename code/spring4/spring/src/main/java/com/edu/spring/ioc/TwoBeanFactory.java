package com.edu.spring.ioc;

import org.springframework.beans.factory.FactoryBean;

//对象创建工厂,继承方式
public class TwoBeanFactory implements FactoryBean<TwoBean> {

    //创建对象
    public TwoBean getObject() throws Exception {
        //return () -> {};//使用此Lambda表达式,返回必须是接口类型
        return new TwoBean();
    }

    //创建什么类型的对象
    public Class<?> getObjectType() {
        return TwoBean.class;
    }

    //是否是单例
    public boolean isSingleton() {
        return true;
    }
}
