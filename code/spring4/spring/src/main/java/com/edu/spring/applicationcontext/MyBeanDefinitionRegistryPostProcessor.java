package com.edu.spring.applicationcontext;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.stereotype.Component;

/**
 * BeanDefinitionRegistry:向注册表中注册 BeanDefinition,
 * 即动态向容器中注入Bean 对象
 */
@Component
public class MyBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {

    //BeanDefinitionRegistryPostProcessor可以拿到以下这两个对象
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory arg0) throws BeansException {
    }

    //可以动态注入Bean到容器中
    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        for (int i = 0; i < 10; i++) {
            //动态创建Bean的定义(即Person类)
            BeanDefinitionBuilder bdb = BeanDefinitionBuilder.rootBeanDefinition(Person.class);
            //给Bean添加属性
            bdb.addPropertyValue("name", "admin" + i);
            //注册Bean,即添加到Spring容器中
            registry.registerBeanDefinition("person" + i, bdb.getBeanDefinition());
        }
    }

}
