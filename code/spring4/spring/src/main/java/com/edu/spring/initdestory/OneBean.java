package com.edu.spring.initdestory;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

//继承方式
public class OneBean implements InitializingBean, DisposableBean {
    //对象销毁后执行
    @Override
    public void destroy() throws Exception {
        System.out.println("对象销毁之后-------");
    }

    // afterPropertiesSet :所有属性被初始化之后执行,但是会在init之前执行
    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("所有属性被初始化之后---------");
    }
}
