package com.edu.spring.ioc;

//工厂类.自定义方式
public class ThreeBeanFactory {

    public ThreeBean create() {
        return new ThreeBean();
    }
}
