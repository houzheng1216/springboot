package com.edu.spring.initdestory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class ThreeBean {

    @PostConstruct   //对象初始化执行
    public void init() {
        System.out.println("对象三初始化-------");
    }

    @PreDestroy   //对象销毁后执行
    public void destroy() {
        System.out.println("对象三销毁-------");
    }

}
