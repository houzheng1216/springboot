package com.edu.spring.ioc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

/**
 * 配置类
 */
@Configuration    //@Configuration:表示此类为配置类,相当于xml配置文件
public class MyConfig {

    //配置bean,直接对象方式
    @Bean(name = "myBean") //@Bean:相当于Bean标签
    @Scope("prototype")  //默认是单例,可以使用此注解prototype改为非单例
    public MyBean getMyBean() {
        return new MyBean();
    }

    //配置bean工厂 ,继承,返回工厂中的bean
    @Bean(name = "twoBeanFactory") //@Bean:相当于Bean标签
    public TwoBeanFactory getTwoBeanFactory() {
        return new TwoBeanFactory();
    }

    //配置bean工厂 自定义,返回工厂bean
    @Bean(name = "threeBeanFactory") //@Bean:相当于Bean标签
    public ThreeBeanFactory getThreeBeanFactory() {
        return new ThreeBeanFactory();
    }

    @Bean(name = "threeBean") //通过工厂Bean参数传递到这,调用工厂方法创建bean
    public ThreeBean getThree(ThreeBeanFactory threeBeanFactory) {
        return threeBeanFactory.create();
    }

    @Bean(name = "userDao")
    @Primary   //表示在创建Bean的时候如果有 过个配置,优先采用有此注解的配置,否则会报错.作用在Bean配置上
    public UserDao getUserDao() {
        return new UserDao();
    }

    @Bean(name = "getUserDao")
    public UserDao createUserDao() {
        return new UserDao();
    }


}
