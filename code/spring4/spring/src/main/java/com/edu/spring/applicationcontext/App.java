package com.edu.spring.applicationcontext;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext("com.edu.spring.applicationcontext");
        //根据类型获取所有Bean对象,并迭代输出
        context.getBeansOfType(Person.class).values().forEach(System.out::println);

        //注册Bean,同样需要
        //动态创建Bean的定义(即Person类)
        BeanDefinitionBuilder bdb = BeanDefinitionBuilder.rootBeanDefinition(Person.class);
        //给Bean添加属性
        bdb.addPropertyValue("name", "admin");
        context.registerBeanDefinition("person", bdb.getBeanDefinition());
        OneBean oneBean = context.getBean(OneBean.class);
        oneBean.show();
        TwoBean twoBean = context.getBean(TwoBean.class);
        twoBean.show();
        ThreeBean threeBean = context.getBean(ThreeBean.class);
        threeBean.show();
        context.close();
    }

}
