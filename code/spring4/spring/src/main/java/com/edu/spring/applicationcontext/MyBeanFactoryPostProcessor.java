package com.edu.spring.applicationcontext;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

@Component
public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

    // 容器初始化操作接口 :BeanFactoryPostProcessor
    // 容器初始化时执行,且只执行一次,在beanPostProcessor之前执行,执行的最早
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        System.out.println("----------" + beanFactory.getBeanDefinitionCount());//获取容器中bean数量
    }

}
