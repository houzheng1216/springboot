package com.edu.spring.initdestory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置类
 */
@Configuration    //@Configuration:表示此类为配置类,相当于xml配置文件
public class MyConfig {

    //配置Bean
    @Bean(name = "oneBean")
    public OneBean getOneBean() {
        return new OneBean();
    }

    // 配置bean时,指定初始化方法和销毁方法
    @Bean(name = "twoBean", initMethod = "init", destroyMethod = "destroy")
    public TwoBean getTwoBean() {
        return new TwoBean();
    }

    //配置Bean
    @Bean(name = "threeBean")
    public ThreeBean getThreeBean() {
        return new ThreeBean();
    }

}
