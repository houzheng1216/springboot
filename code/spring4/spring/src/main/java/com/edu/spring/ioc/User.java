package com.edu.spring.ioc;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.edu.spring.ioc.MyBean;

/**
 * @Component //加入容器中,默认名字为类名,首字母小写,一般是没有明确角色时使用
 * 类似的还有很多 :  @Repository   一般在dao层
 * @Service 一般在业务逻辑层使用
 * @Controller 一般在控制层使用
 */
@Component    //加入容器中,默认名字为类名,首字母小写
public class User {
    //	@Qualifier("userDao") /表示在创建Bean的时候如果有 多个,优先指定一个执行,否则报错,作用在注入的属性上
    @Autowired     //依赖注入:将UserDao对象注入到User的属性中,不需要set,get方法
    private UserDao userDao;

    @Resource   //使用Resource注入
    private MyBean myBean;

    /*@Inject  //使用Inject注解注入,需要导入相关依赖或者jar包
    private TwoBean twoBean;
*/
    @Override
    public String toString() {
        return "User [userDao=" + userDao + ", oneBean=" + myBean + "]";
    }


}
