package com.edu.spring.ioc;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 */
public class App {
    public static void main(String[] args) {
        /**
         * 使用AnnotationConfigApplicationContext可以实现基于Java的配置类加载Spring的应用上下文.
         * 避免使用application.xml进行配置。
         * 在使用spring框架进行服务端开发时，注解配置在便捷性，和操作上都优于是使用XML进行配置;
         */
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MyConfig.class, User.class);
        /**
         * 当一个一个类加入构造函数时太麻烦,还可以直接扫描某一个包,如下
         */
        //AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext("cn.edu.spring");
        //根据配置类获取
        /**
         * 还可以创建一个扫描类,在类上写上配置类注解,并加入@CompomentScan("cn.edu.sprinh")
         * 在构造函数时,传入此类参数 ,同样可以达到扫描效果,并且可以排除某些类:,还可以使用include包含一些其他类
         * 扫描包时,过滤包下的某些类,不加入容器中                                                                                                           //根据类型排除,只能写配置类或者直接在类上使用注解的类,不能下配置类中的Bean类
         * @CompomentScan("cn.edu.sprinh",excludeFilters=@Filter(FilterType.ASSINGSBLE_TYPE,classes={MyConfig.class,UserDao.class}))
         */
        MyBean myBean = context.getBean(MyBean.class);//根据类获取
        MyBean myBean1 = (MyBean) context.getBean("myBean");//根据名字获取,默认是配置类的方法名,可在属性里指定
        System.out.println(myBean);
        System.out.println(myBean1);

        //根据FactoryBean获取获取工厂中的bean.继承工厂
        TwoBean twoBean1 = (TwoBean) context.getBean("twoBeanFactory");
        TwoBean twoBean2 = context.getBean(TwoBean.class);
        //获取工厂本身bean
        TwoBeanFactory twoBeanFactory = (TwoBeanFactory) context.getBean(TwoBeanFactory.class);
        System.out.println(twoBean1);
        System.out.println(twoBean2);
        System.out.println(twoBeanFactory);

        //根据FactoryBean获取获取工厂中的bean,自定义的工厂
        ThreeBeanFactory threeBeanFactory = context.getBean(ThreeBeanFactory.class);
        System.out.println(threeBeanFactory);
        //获取bean,通过自定义工厂 
        ThreeBean threeBean = context.getBean(ThreeBean.class);
        System.out.println(threeBean);

        //获取直接的user对象Bean
        User user = context.getBean(User.class);
        System.out.println(user);

        context.close();
    }
}
