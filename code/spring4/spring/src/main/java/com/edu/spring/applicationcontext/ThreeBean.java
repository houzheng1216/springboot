package com.edu.spring.applicationcontext;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * 方法三:spring4.3新特性:
 * 使用构造函数参数方法,需要注意:
 * 只能有一个构造函数,如果有多个构造函数,Spring会默认调用无参的构造器,没有就报错
 * 此时,就无法获取applicationContext属性
 */
@Component
public class ThreeBean {

    private ApplicationContext applicationContext;

    //构造函数中传入参数会自动依赖注入
    public ThreeBean(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void show() {
        System.out.println("----" + applicationContext);
    }


}
