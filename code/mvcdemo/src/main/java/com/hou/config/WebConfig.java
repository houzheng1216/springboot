package com.hou.config;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.ArrayList;


@Configurable  //声明配置类
@ComponentScan(value ="com.*",includeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION,value = Controller.class)})
@EnableWebMvc  //启动springmvc配置
public class WebConfig {

    /**
     * 视图解析器
     */
    @Bean(name="internalResourceViewResolver")
    public ViewResolver initViewResolver(){
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/jsp/");//设置跳转页面路径
        viewResolver.setSuffix(".jsp"); //页面后缀
        return viewResolver;
    }

    /**
     * 请求映射处理适配器
     */
    @Bean(name="requestMappingHandlerAdapter")
    public HandlerAdapter initRequestHandlerAdapter(){
        RequestMappingHandlerAdapter handlerAdapter = new RequestMappingHandlerAdapter();
        //配置JSON转换器
        MappingJackson2XmlHttpMessageConverter messageConverter = new MappingJackson2XmlHttpMessageConverter();
        ArrayList<MediaType> mediaTypes = new ArrayList<MediaType>();
        mediaTypes.add(MediaType.APPLICATION_JSON_UTF8);//设置接受JOSN转换
        messageConverter.setSupportedMediaTypes(mediaTypes);
        handlerAdapter.getMessageConverters().add(messageConverter);//适配器添加JSON转换
        return handlerAdapter;
    }
}
