package com.hou.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

//配置DispatcherServlet
public class WebInit extends AbstractAnnotationConfigDispatcherServletInitializer {

    //配置AOP容器,返回AOP配置文件
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[0];
    }
    //url映射配置,返回spring的配置文件,
    //这里WebConfig主要是配置DispatcherSerlvet,视频解析器,JSON等
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebConfig.class};
    }
    //拦截请求匹配,只拦截.do
    @Override
    protected String[] getServletMappings() {
        return new String[]{"*.do"};
    }
}
