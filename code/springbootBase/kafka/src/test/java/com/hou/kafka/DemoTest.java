package com.hou.kafka;

import org.junit.Test;

public class DemoTest {

    @Test
    public void test01(){
        Integer a=127;
        Integer b=127;
        System.out.println(a==b);// true  自动装箱会使用缓存中同一个对象

        Integer d=new Integer(127);
        Integer e=new Integer(127);
        System.out.println(d==e);   // false 使用new每次都是一个新对象

        Integer f=Integer.valueOf(127);
        Integer n=Integer.valueOf(127);
        System.out.println(f==n); // true  valueOf方法会使用缓存中的对象  [-128,hign(默认127)] 之间都是同一个对象
        Integer c=129;
        System.out.println(a==c); // false  不同对象
        System.out.println(a==b.intValue()); //true Integer会拆箱转为int,进行比较数值
        System.out.println(a.equals(b)); //true  比较基本数值的值
    }

    public static char charAt(byte[] value, int index) {
        index <<= 1;
        if (index < 0 || index >= value.length) {
            throw new StringIndexOutOfBoundsException(index);
        }
        /**
         * value[index] & 0xff  :  结果为int,即byte -> int
         * 16进制数 0xff 二进制就是11111111 ,高位都为0
         * 因为&操作中，超过0xff的部分，全部都会变成0，而对于0xff以内的数据，
         * 它不会影响原来的值,取其最低8位,即一个字节的值,此操作也能保证负数补码不变
         */
        System.out.println(value[index]);
        final int i = value[index] & 0xff;
        System.out.println(i);
        return (char)i;
    }



    @Test
    public void test03(){
        final long l1 = -2;
        System.out.println(l1);
        System.out.println(Integer.toUnsignedLong(-2));
    }

    @Test
    public void test04(){
        String a="a";
        System.out.println(a.charAt(0));
        System.out.println(a.codePointAt(0));

        String b="汉";
        System.out.println(b.charAt(0));
        System.out.println(b.codePointAt(0));
    }
}
